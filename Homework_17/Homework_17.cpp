// Homework_17.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <math.h>

using namespace std;

class Point
{
private:

    double x;
    double y;
    double z;

public:
    double GetX()
    {
        return x;
    }
    void SetX(double valX)
    {
        x = valX;
    }


    double GetY()
    {
        return y;
    }

    void SetY(double ValY)
    {
        y = ValY;
    }

    double GetZ()
    {
        return z;
    }

    void SetZ(double ValZ)
    {
        z = ValZ;
    }
    void PrintVector()
    {
        cout << "Values of vector are: " << "\nX " << GetX() << "\nY " << GetY() << "\nZ " << GetZ() << endl;
    }
    double length(double x, double y, double z)
    {
        return sqrt(x * x + y * y + z * z);
    }

};



int main()
{
   
    Point Vector;
    Vector.SetX(2.43);
    double resX = Vector.GetX();
    
    Vector.SetY(3.14);
    double resY = Vector.GetY();

    Vector.SetZ(4.11);
    double resZ = Vector.GetZ();

    Vector.PrintVector();

    cout << endl;
    cout << "The length of the vector is: " << Vector.length(resX, resY, resZ) << endl;


    return 0;
}

